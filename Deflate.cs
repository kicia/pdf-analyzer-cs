
class Deflate {

    public static byte[] trimStart2( byte[] buffer ){
        uint trimCount = 2;

        // -- error
        if( buffer.Length < trimCount + 1 ){
            Console.WriteLine( "Length is < {0}: {1}", trimCount + 1, buffer.Length );
            return new byte[0];
        }

        // -- trim bytes
        byte[] trimmedBytes = new byte[ buffer.Length - trimCount ];
        for( uint i = 0; i < trimmedBytes.Length; i++ ){
            trimmedBytes[ i ] = buffer[ i + trimCount ];
        }
        return trimmedBytes;
    }

    public static byte[] deflate( byte[] buffer ){
        byte[] trimmedBytes = trimStart2( buffer );

        // -- error
        if( trimmedBytes.Length < 1 ){
            return new byte[0];
        }

        // --
        Stream stream = new MemoryStream( trimmedBytes );
        using var decompressor = new System.IO.Compression.DeflateStream(
            stream,
            System.IO.Compression.CompressionMode.Decompress
        );

        // -- stream to byte[]
        MemoryStream targetBuffer = new MemoryStream();
        decompressor.CopyTo( targetBuffer );
        return targetBuffer.ToArray();
    }
}