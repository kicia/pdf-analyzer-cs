class TestDeflate {
    public static void test(){

        string HOME = Environment.GetFolderPath( Environment.SpecialFolder.UserProfile );
        string filePath = HOME + "/Documents/out_1";

        byte[] bytes = FileSystem.Read( filePath );
        if( bytes.Length < 2 ){
            Console.WriteLine( "Length is < 2: {0}", bytes.Length );
            return;
        }
        byte[] bytesMinus1 = new byte[ bytes.Length - 2 ];
        for( uint i = 0; i < bytesMinus1.Length; i++ ){
            bytesMinus1[ i ] = bytes[ i + 2 ];
        }

        byte[] decompressed = Deflate.deflate( bytesMinus1 );

        FileSystem.Write( HOME + "/Documents/out_test", decompressed );
   		//FileSystem.Write( HOME + "/Documents/out_test", bytesMinus1 );
    }
}