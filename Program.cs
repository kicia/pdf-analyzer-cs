﻿class Program
{
    static bool compareStrings( string A, string B ){
        if( A.Length != B.Length ){
            return false;
        }
        for( int i = 0; i < A.Length; i++ ){
            if( A[i] != B[i] ){
                return false;
            }
        }
        return true;
    }

    static void Main(string[] args)
    {
        // --
        if( args.Any() && compareStrings( args[0], "test" ) ){
            Console.WriteLine("Test");
            //TestDeflate.test();
            return;
        }

        // -- get file path from command line
        string filePath = getFilePath( args );
        if( filePath.Length < 1 ){
            Console.WriteLine("canceled");
            return;
        }

        // -- read file
        byte[] bytes = FileSystem.Read( filePath );
        Console.WriteLine("File loaded: {0} {1}", bytes.Length, filePath);
        Pdfparser pdfparser = new Pdfparser( bytes );
    }

    // -- get file path from command line
    static string getFilePath( string[] args ){

        if( !args.Any() )
        {
            Console.WriteLine("No file specified.");
            return string.Empty;
        }

        string path = args[0];

        if( !File.Exists( path ) )
        {
            Console.WriteLine("File not found: {0}", path);
            return string.Empty;
        }

        return path;
    

    }
    
}
