class ByteStrings {
    public static byte[] stream       = { 115, 116, 114, 101, 97, 109 };
    public static byte[] endstream    = { 101, 110, 100, 115, 116, 114, 101, 97, 109 };
    public static byte[] obj          = { 111, 98, 106 };
    public static byte[] trailer      = { 116, 114, 97, 105, 108, 101, 114 };
    public static byte[] xref         = { 120, 114, 101, 102 };
    public static byte[] startxref    = { 115, 116, 97, 114, 116, 120, 114, 101, 102 };
    public static byte[] endobj       = { 101, 110, 100, 111, 98, 106 };
    public static byte[] PDFSignature = { 37, 80, 68, 70 };
    public static byte[] EOF          = { 37, 37, 69, 79, 70 }; // "%%EOF"
    public static byte[] nullValue    = { 110, 117, 108, 108 }; // "null"
    public static byte[] DictStart    = { 60, 60 };  // "<<"
    public static byte[] DictEnd      = { 62, 62 };  // ">>"
}