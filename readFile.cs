using System;
using System.IO;
using System.Text;

class FileSystem
{
    public static byte[] Read( string path )
    {
        // -- error: not found
        if( !File.Exists( path ) )
        {
            Console.WriteLine("File not found: {0}", path);
            return new byte[0];
        } else {
            Console.WriteLine("File found: {0}", path);
        }

        // -- Open the file to read from.
        using ( FileStream fileStream = File.Open( path, FileMode.Open, FileAccess.Read ) )
        {
            byte[] buffer = new byte[ fileStream.Length ];
            UTF8Encoding temp = new UTF8Encoding( true );
            int readLen;
            
            readLen = fileStream.Read( buffer, 0, buffer.Length );
            if( readLen < 1 ){
                Console.WriteLine("Unexpected error loading file: {0}", path);
                return new byte[0];
            }

            // Console.WriteLine( temp.GetString( b, 0, readLen ) );

            return buffer;
        }
    }
    public static bool Write( string path, byte[] bytes ){
        
        Console.WriteLine( "write: {0} ...", bytes.Length );

        // --
        using ( FileStream fileStream = File.Open( path, FileMode.Create, FileAccess.Write ) )
        {
            fileStream.Write( bytes, 0, bytes.Length );
            return true;
        }

        // -- TODO: Retrun true on success, false on error
        //return false;
    }
}
