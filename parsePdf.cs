// ===============================================================================
//
// Parse a PDF file
//
// TODO:
//  - generate tokens, send to callback
//  - handle strings (e.g. "(my string)")
//  - Check and harmonize the display of the pointer of a found token,
//    i.e. "found token at ptr x" vs. "found token, new ptr after the token is x"
//  - Check and harmonize "consume", "skip", "is..." (incBy or not, and at which point)
//  - parse dictionary keys / values
//  - Check if `/Filter /FlateDecode` before deflating
//
// ===============================================================================

using System.Text;

class Pdfparser {

    // -------------------------
    // properties
    // -------------------------

    private string writePath = Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ) + "/Documents/out";

    private uint ptr = 0;
    byte[] allBytes = {};
    private uint filesCount = 0;

    public enum Token
    {
        PdfHead,
        StreamStart,
        StreamEnd,
        StreamContent,
        EndObject,
        ObjectId,
        ObjectN2,
        ObjectStart,
    }

    // -------------------------
    // constructor
    // -------------------------
    public Pdfparser( byte[] allBytes ){
        this.allBytes = allBytes;
        this.ptr = 0;
        this.Parse();
    }

    // -------------------------
    // methods
    // -------------------------

    uint intToUint( int n ){
        if( n < 0 ){
            Console.WriteLine( "Unexpected error: uint < 0: {0}", n );
            return 0;
        }

        return (uint) n;
    }

    // -------------------------
    // pointer methods
    // -------------------------

    bool atEnd(){
        return this.ptr >= allBytes.Length;
    }

    uint inc(){
        this.ptr++;
        return this.ptr;
    }
	
	bool lookAheadNumber(){ // next character is dot or integer
		if( this.ptr + 1 >= this.allBytes.Length ){
			return false;
		}

		byte b = this.getByte();
		return (b > 47 && b < 58) || b == 46;
    }

    uint incBy( uint n ){
        this.ptr += n;
        uint len = this.intToUint( allBytes.Length );
        if( this.ptr >= len ){
            this.ptr = len;
        } 
        return this.ptr;
    }

    // -------------------------
    // basic tokens
    // -------------------------

    bool isWS(){
        byte b = this.getByte();
        return b == 32 || b == 9 || b == 10 || b == 13;
    }

    bool isDigit(){
        byte b = this.getByte();
        return b > 47 && b < 58;
    }

    bool isDot(){
        byte b = this.getByte();
        return b == 46;
    }

    bool isNumberSign(){
        byte b = this.getByte();
        return b == 43 || b == 45;
    }

    uint consumeInteger(){
		if( !this.isDigit() ){
			return 0;
		}

		uint startPtr = this.ptr;

        while( this.isDigit() ){
			this.inc();
		}

		return this.ptr - startPtr;
    }

    uint consumeRealNumber(){
		
		bool dotFound = false;
		uint startPtr = this.ptr;
		
		if( this.isNumberSign() ){            // "+" or "-"
			if( !this.lookAheadNumber() ){    // Not a number after the sign
				return 0;
			}
			this.inc();
		}

        while( !this.atEnd() && ( this.isDigit() || (!dotFound && this.isDot()) ) ){
			if( this.isDot() ){
				dotFound = true;
			}
			this.inc();
		}

		return this.ptr - startPtr;
    }

    bool isBr(){
        byte b = this.getByte();
        return b == 10 || b == 13;
    }

    bool skipWS(){
        if( !this.isWS() ){ return false; }
        while( this.isWS() ){ this.inc(); }
        return true;
    }

    bool skipBr(){
        if( !this.isBr() ){ return false; }
        while( this.isBr() ){ this.inc(); }
        return true;
    }
    
    void skipLine(){
        while( !this.atEnd() && !this.isBr() ){
            this.inc();
        }
        while( this.isBr() ){
            this.inc();
        }
    }

    byte getByte(){
        if( this.ptr < allBytes.Length ){
            return allBytes[ this.ptr ];
        }
        return 0;
    }

    // -------------------------
    // specific tokens
    // -------------------------

    uint isSpecificString( byte[] expected ){

        uint expectedLen = this.intToUint( expected.Length );

        for( uint i = 0; i < expectedLen; i++ ){
            uint ptr = this.ptr + i;
            if( ptr >= this.allBytes.Length || this.allBytes[ ptr ] != expected[i] ){
                return 0;
            }
        }

        return expectedLen;
    }

    // -------------------------
    // 
    // -------------------------
    private uint expectDocStart(){
        uint startPtr = this.ptr;
        uint len = this.isSpecificString( ByteStrings.PDFSignature );
        if( len < 1 ){
            return 0;
        }

        while( !this.atEnd() && !this.skipBr() ){
            this.inc();
        }
        
        while( this.optionalPdfHead() ){};
		
        return this.ptr - startPtr;
    }

    private uint expectObjectStart(){
		uint startPtr = this.ptr;
		uint n1 = consumeInteger();
		if( n1 < 1 ){ return 0; }
		this.skipWS();
		uint n2 = consumeInteger();
		if( n2 < 1 ){ return 0; }
		this.skipWS();
		uint obj = this.isSpecificString( ByteStrings.obj );
		if( obj < 1 ){ return 0; }
		this.incBy( obj );
		return this.ptr - startPtr;
	}

    private uint expectXrefMark(){
		uint startPtr = this.ptr;
		uint found = this.isSpecificString( ByteStrings.xref );
		if( found < 1 ){ return 0; }
		this.incBy( found );
		return this.ptr - startPtr;
	}

    private uint expectTrailer(){
		uint startPtr = this.ptr;
		uint found = this.isSpecificString( ByteStrings.trailer );
		Console.WriteLine(".. try trailer: {0} {1}", found, this.ptr);
		if( found < 1 ){ return 0; }
		this.incBy( found );
		return this.ptr - startPtr;
	}

    private uint expectStartXref(){
		uint startPtr = this.ptr;
		uint found = this.isSpecificString( ByteStrings.startxref );

		while( !this.atEnd() && found < 1 ){
			this.inc();
			found = this.isSpecificString( ByteStrings.xref );
		}

		if( found < 1 ){ // not found
			return 0;
		}

		this.incBy( found );

		return this.ptr - startPtr;
	}

    private void exportStreamToFile( uint startPtr, uint writeLength ){
		this.filesCount++;
		Console.WriteLine( "(endstream) {0} {1} {2}", this.filesCount, startPtr, writeLength );

		// -- copy bytes
		byte[] bytesToWrite = new byte[ writeLength ];
		for( uint i = 0; i < writeLength; i++ ){
			bytesToWrite[i] = allBytes[ startPtr + i ];
		}

		// -- write file
		FileSystem.Write( this.writePath + "_" + this.filesCount.ToString(), bytesToWrite );

		// -- deflate
		// TODO: Check if `/Filter /FlateDecode` first
		// byte[] decoded = Deflate.deflate( bytesToWrite );
		// FileSystem.Write( this.writePath + "_decoded_" + this.filesCount.ToString(), decoded );
	}

    private uint consumeStream(){
		this.skipBr();
		uint startStreamPtr = this.ptr;
		
		// -- find endstream
		uint foundEndStream = this.isSpecificString( ByteStrings.endstream );
		while( !this.atEnd() && foundEndStream < 1 ){
			this.inc();
			foundEndStream = this.isSpecificString( ByteStrings.endstream );
		}

		if( foundEndStream > 0 ){
			this.exportStreamToFile( startStreamPtr, this.ptr - startStreamPtr );
			this.incBy( foundEndStream );
			return this.ptr - startStreamPtr;
		}

		// -- "endstream" not found
		return 0;
	}

    private bool checkStream(){
		uint startPtr = this.ptr;
		uint foundStream = this.isSpecificString( ByteStrings.stream );

		// -- "stream" not found
		if( foundStream < 1 ){
			 return false;
		}

		// -- "stream" found
		this.incBy( foundStream );
		foundStream = this.consumeStream();
		if( foundStream < 1 ){
			// error: "endstream" not found
            Console.WriteLine( "Error: expected 'endobj' not found: {0} {1}", this.ptr, foundStream );
			return false;
		}

		return true;
	}

    private uint expectUpToEndObject(){
		uint startPtr = this.ptr;
		
		uint foundEndObject = this.isSpecificString( ByteStrings.endobj );

		// -- "endobj"
		while( !this.atEnd() && foundEndObject < 1 ){
			if( !this.checkStream() ){
				this.inc();
			};
			foundEndObject = this.isSpecificString( ByteStrings.endobj );
		}

		// -- "endobj" not found
		if( foundEndObject < 1 ){
			return 0;
		}

		// -- "endobj" found
		this.incBy( foundEndObject );
		return this.ptr - startPtr;
	}
    private uint skipNonDictionaryMarker(){
		uint startPtr = this.ptr;
		uint foundLen = 0;

		while( !this.atEnd() ){
			// -- dictionary start ("<<")
			foundLen = this.isSpecificString( ByteStrings.DictStart );
			if( foundLen > 0 ){
				return this.ptr - startPtr;
			}

			// -- dictionary end (">>")
			foundLen = this.isSpecificString( ByteStrings.DictEnd );
			if( foundLen > 0 ){
				return this.ptr - startPtr;
			}
			this.inc();
		}

		// -- error: no dictionary start or end found
		return 0;
	}

    private bool optionalPdfHead(){
        if( this.getByte() == 37 ){
            this.skipLine();
			return true;
        } else {
			return false;
		}
    }

    // -------------------------
    // start parsing
    // -------------------------

    public void Parse(){
        State state = new State();
        state.push( State.StateId.DocStart );

		uint foundTokenLen = 0;

        while( !this.atEnd() ){
            
            State.StateId currentState = state.get();
			//Console.WriteLine("-- stack: {0}, ptr: {1}, state: {2}", state.Length, this.ptr, currentState);

            switch( currentState ){
                case State.StateId.DocStart:
				    Console.WriteLine("State.StateId.DocStart: {0}", this.ptr);

                    foundTokenLen = this.expectDocStart();
					if( foundTokenLen > 0 ){
		                Console.WriteLine("PDF head: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
						state.replace( State.StateId.ObjectStart );
						continue;
					}
    
					state.replace( State.StateId.Error );
                    continue;

                case State.StateId.ObjectStart: // "obj" || "xref" || "trailer"
				    Console.WriteLine("State.StateId.ObjectStart: {0}", this.ptr);

					this.skipWS();

					// -- "obj"
                    foundTokenLen = this.expectObjectStart();
					if( foundTokenLen > 0 ){
	                    Console.WriteLine("obj: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
						state.replace( State.StateId.ObjectContent );
						continue;
					}

					// -- "xref"
                    foundTokenLen = this.expectXrefMark();
					if( foundTokenLen > 0 ){
						state.replace( State.StateId.StartXref );
						continue;
					}

					// -- "trailer"
                    foundTokenLen = this.expectTrailer();
					if( foundTokenLen > 0 ){
	                    Console.WriteLine("trailer: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
						state.replace( State.StateId.OptionalStartXref );
						state.push( State.StateId.DictionaryStart );
						continue;
					}

					// -- error
	                Console.WriteLine("Error: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
					state.replace( State.StateId.Error );
                    continue;

                case State.StateId.StartXref:
					foundTokenLen = this.expectStartXref();
				    Console.WriteLine("State.StateId.StartXref: {0} {1}", this.ptr, foundTokenLen);
					if( foundTokenLen > 0 ){
						this.skipWS();
						foundTokenLen = this.consumeInteger();
						if( foundTokenLen > 0 ){
							state.replace( State.StateId.EOF );
							continue;
						}
					}

					// -- error
					state.replace( State.StateId.Error );
					continue;
					
				case State.StateId.OptionalStartXref:
					foundTokenLen = this.isSpecificString( ByteStrings.startxref );
					if( foundTokenLen > 0 ){
					    Console.WriteLine("State.StateId.StartXref: {0} {1}", this.ptr, foundTokenLen);
						this.incBy( foundTokenLen );
						this.skipWS();
						foundTokenLen = this.consumeInteger();
					}

					// --
					state.replace( State.StateId.EOF );
					continue;

				case State.StateId.ObjectContent:
					Console.WriteLine("State.StateId.ObjectContent: {0}", this.ptr);

					this.skipWS();

					// -- "endobj"
					foundTokenLen = this.isSpecificString( ByteStrings.endobj );
					if( foundTokenLen > 0 ){
						Console.WriteLine("endobj: {0}", this.ptr);
						this.incBy( foundTokenLen );
						state.replace( State.StateId.ObjectStart );
						continue;
					}
					
					// -- "stream"
					bool streamFound = this.checkStream();
					if( streamFound ){
						Console.WriteLine("stream: {0}", this.ptr);
						state.replace( State.StateId.EndObject );  // "endstream" should always be followed by "endobj"
						continue;
					}

					// -- "null"
					foundTokenLen = this.isSpecificString( ByteStrings.nullValue );
					if( foundTokenLen > 0 ){
						Console.WriteLine("null: {0}", this.ptr);
						this.incBy( foundTokenLen );
						state.replace( State.StateId.EndObject );
						continue;
					}
					
					// -- number
					foundTokenLen = this.consumeRealNumber();
					if( foundTokenLen > 0 ){
						Console.WriteLine("number: {0}", this.ptr);
						state.replace( State.StateId.EndObject );
						continue;
					}

					// -- string
					// foundTokenLen = this.consumeString(); // TODO

					// -- dictionary
					foundTokenLen = this.isSpecificString( ByteStrings.DictStart );
					if( foundTokenLen > 0 ){
						// --
						// TODO:
						//  Often a dictionary is followed by a stream.
						//  Can an object generally contain multiple values, or is that special for streams ?
						// --
						state.replace( State.StateId.ObjectContent );
						state.push( State.StateId.DictionaryContent );
						this.incBy( foundTokenLen );
						Console.WriteLine("dictionary start: stack: {0}, ptr: {1}", state.Length, this.ptr);
						continue;
					}

					// -- error
					state.replace( State.StateId.Error );
					continue;

				case State.StateId.DictionaryStart:
					this.skipWS();
					foundTokenLen = this.isSpecificString( ByteStrings.DictStart );
					if( foundTokenLen > 0 ){
						this.incBy( foundTokenLen );
						state.replace( State.StateId.DictionaryContent );
						Console.WriteLine("dictionary start: stack: {0}, ptr: {1}", state.Length, this.ptr);
						continue;
					}

					// -- error
					state.replace( State.StateId.Error );
					continue;

				case State.StateId.DictionaryContent:

					this.skipWS();

					// -- dictionary start
					foundTokenLen = this.isSpecificString( ByteStrings.DictStart );
					if( foundTokenLen > 0 ){
						this.incBy( foundTokenLen );
						state.push( State.StateId.DictionaryContent );
						Console.WriteLine("dictionary start: stack: {0}, ptr: {1}", state.Length, this.ptr);
						continue;
					}

					// -- dictionary end
					foundTokenLen = this.isSpecificString( ByteStrings.DictEnd );
					if( foundTokenLen > 0 ){
						this.incBy( foundTokenLen );
						Console.WriteLine("dictionary end: stack: {0}, ptr: {1}", state.Length, this.ptr);
						State.StateId tempState = state.pop();
						Console.WriteLine("           ...: stack: {0}, ptr: {1}, state: {2}-->{3}", state.Length, this.ptr, currentState, state.get());
						continue;
					}

					// -- not dictionary start or end
					foundTokenLen = this.skipNonDictionaryMarker();
					if( foundTokenLen > 0 ){
						// -- now the pointer is at a "<<" or a ">>", which are not yet consumed.
						Console.WriteLine("dictionary content: stack: {0}, ptr: {1}", state.Length, this.ptr);
						continue;
					}

					Console.WriteLine("## DictionaryContent, Error: {0}, ptr: {1}, state: {2}", state.Length, this.ptr, currentState);

					// -- error: no dictionary start or end found
					state.replace( State.StateId.Error );
					continue;

                case State.StateId.EOF:
					Console.WriteLine("State.StateId.EOF: {0}", this.ptr);
					this.skipWS();
					foundTokenLen = this.isSpecificString( ByteStrings.EOF );
					if( foundTokenLen > 0 ){
	                    Console.WriteLine("EOF: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
						this.incBy( foundTokenLen );
						return;
					}

					state.replace( State.StateId.Error );
					continue;

                case State.StateId.EndObject:
					Console.WriteLine("State.StateId.EndObject: {0}", this.ptr);
					this.skipWS();

					foundTokenLen = this.isSpecificString( ByteStrings.endobj );
					if( foundTokenLen > 0 ){
	                    Console.WriteLine("EndObject: len: {0}, ptr: {1}", foundTokenLen, this.ptr);
						this.incBy( foundTokenLen );
						state.replace( State.StateId.ObjectStart );
						continue;
					}

					state.replace( State.StateId.Error );
					continue;

                case State.StateId.Error:

					// -- create context string
					byte[] context = new byte[30];
					uint i = 0;
					for( i = 0; i < 30; i++ ){
						if( this.ptr > 7 ){
							uint logPtr = this.ptr + i - 8;
							if( logPtr >= 0 && logPtr < this.allBytes.Length ){
								context[i] = this.allBytes[ logPtr ];
							} else {
								context[i] = 46;
							}
						} else {
							context[i] = 46;
						}
					}
					
					// --
					Console.WriteLine("!!!!! Parsing, Error: {0}, ptr: {1}, state: {2}, context:\n\"{3}\"", state.Length, this.ptr, currentState, Encoding.Default.GetString(context) );
					return;

                default:
					Console.WriteLine("State: {0}, ptr: {1}", currentState, this.ptr);
                    return;
            }
        }
		
		if( state.get() != State.StateId.Done ){
			Console.WriteLine("!!!!! Unexpected end of file: ptr: {0}, state: ({1}) {2}", this.ptr, state.Length, state.get() );
		}
    }
}