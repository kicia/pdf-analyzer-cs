# questions

- Should methods like `isWs` all be `private` ?
- Can I create a byte array of e.g. "endstream" ?
- `len` can not be used in `case State.StateId.DocStart` ?