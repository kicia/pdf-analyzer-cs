class State {

    public State(){
    }
    private static uint MAXSTACKLENGTH = 1000;

    public enum StateId {
        None,               // "not set" value
		Error,
        DocStart,           // start of PDF document
        ObjectStart,        // e.g. "1 0 obj"
        ObjectContent,      // object value, e.g. number, string, dictrionary, null
        EndObject,          // "endobj"
		UpToEndObject,      // everything until and including "endobject"
        DictionaryStart,    // "<<"
        DictionaryContent,
        DictionaryEnd,      // ">>"
        Trailer,            // "trailer"
		XrefMark,           // "xref"
		StartXref,          // "startxref"
        OptionalStartXref,  // optional "startxref"
        PropertyName,       // "/SomeName"
        PropertyArgument,   // e.g. "[0 1 2]"
        Value,              // e.g. "123", "abc", "12.45"
        IntegerValue,  
		EOF,                // "%%EOF"
        Done,               // nothing left to do
    }

    private StateId[] stack = new StateId[ State.MAXSTACKLENGTH ];
    private uint stackLength = 0;

    public StateId get(){
        if( this.stackLength > 0 ){
            StateId returnValue = this.stack[ this.stackLength - 1 ];
            return returnValue;
        }
        return State.StateId.None;
    }
	
	public uint Length {
		get { return this.stackLength; }
    }

    public void push( StateId stateId ){
		
		if( this.stackLength > State.MAXSTACKLENGTH ){
			// TODO: Error: stack overflow
		}

        this.stack[ this.stackLength ] = stateId;
        this.stackLength++;
    }
    
    public StateId pop(){
        if( this.stackLength > 0 ){
            this.stackLength--;
            StateId returnValue = this.stack[ this.stackLength ];
            return returnValue;
        }
        return State.StateId.None;
    }

    public bool replace( StateId stateId ){
        if( this.stackLength > 0 ){
            this.stack[ this.stackLength - 1 ] = stateId;
            return true;
        }
        return false;
    }
}